from kNN import *
from CrossValidation import *
from AlgorithmAnalysis import *


def read_file(file_name):
    # VERIFICA SE O ARQUIVO FOI ENCONTRADO
    while True:
        try:
            file = open(file_name, "r")
            break
        except IOError:
            print("could not open file!")
    # LE O ARQUIVO
    with open(file_name) as file:
        data = []
        for row in file:
            # REMOVE CARACTERES INDESEJADOS E CORTA A LINHA
            row = row.strip().split(';')
            if "NULL" and "null" not in row:
                # CONVERTE CADA DADO DA LINHA DE STR PARA FLOAT
                for j in range(0, len(row)):
                    try:
                        row[j] = float(row[j])
                    except ValueError:
                        print("could not convert {} to float".format(row[j]))
                data.append(row)
            else:
                print("null value found")
    return data

def count_classes(data_set):
    """"
        A maior do maior valor que aparece na tabela sabemos que as classes vao do 0 até o maior_valor
    """
    maior_valor_classe = 0
    for instance in data_set[1:]:
        if instance[-1] > maior_valor_classe:
            maior_valor_classe = int(instance[-1])

    print("As classes vao de {} a {}".format(1, maior_valor_classe))

    maior_valor_classe = maior_valor_classe
    return maior_valor_classe

def get_k(p, q, implementation):
    """  Seja P a quantidade de classes e Q a quantidade de instancias de um conjunto de dados
         A funcao retorna a quantidade de K vizinhos de acordo com a implementação escolhida """

    # CALCULA O M
    if not p % 2:
        m = p + 1
    else:
        m = p

    # CALCULA O VALOR DE K
    if implementation == 1:
        k_neighbors = 1
    elif implementation == 2:
        k_neighbors = m + 2
    elif implementation == 3:
        k_neighbors = m * 10 + 1
    elif implementation == 4:
        value = math.floor(q / 2)
        if not value % 2:
            k_neighbors = value
        else:
            k_neighbors = value + 1

    return k_neighbors


if __name__ == "__main__":

    file = input("Digite o nome do arquivo csv: ")
    if not ".csv" in file:
        file += ".csv"
    print(file)
    data_set = read_file("CSV_Files\\"+file)
    print("\n==================================="
          "\n========= Algoritmo K-NN =========="
          "\n===================================")

    number_of_classes = count_classes(data_set)
    number_of_instances = len(data_set) - 1

    print("\nNumero de instancias: {} \nNumero de classes: {}".
          format(number_of_instances, number_of_classes))
    while True:
        option = input("\nEscolha a opcao desejada:\n\n"
                       "[1] - 1-NN \n[2] - (M+2)-NN \n[3] - (M*10 + 1)-NN "
                       "\n[4] - (Q/2 + 1)-NN ou (Q/2)-NN \n[5] - Fornecer k \n[6] - Sair\n\n")
        try:
            option = int(option)
        except ValueError:
            print("Numero Invalido")
            continue
        if option <= 6:
            break
        else:
            print("Opcao Invalida")

    if option < 5:
        k = get_k(number_of_classes, number_of_instances, option)
    elif option == 5:
        k = input("\nDefina o valor para k: ")

    while True:
        print("Quantidade de K vizinhos: ", k)
        try:
            k = int(k)
            break
        except ValueError:
            print("Numero Invalido")

    training_set = []
    test_set = []

    cv = CrossValidation()
    # GERA AS PARTICOES
    partitions = cv.split_dataset(data_set, 10)

    analysis = AlgorithmAnalysis()

    # CRIA A MATRIZ DE CONFUSAO
    analysis.confusion_matrix(number_of_classes)
    predictions = []

    # CROSS VALIDATION 10-FOLD
    for i in range(len(partitions)):
        print("\n ************************************ FOLD: #{} ************************************".format(i))
        test_set = partitions[i]
        # NOVO TESTE
        for j in range(len(partitions)):
            if i != j:
                # print("antes: ", len(training_set))
                # print("add no treino particao: ", j)
                training_set.extend(partitions[j])
                # print("depois: ", len(training_set))
        # print("\narray treino: ", training_set, "tamanho: \n", len(training_set))
        # print("\narray teste: ", test_set, "tamanho: \n", len(test_set))

        sampling_error = 0
        count = 0
        for instancia_test in test_set:
            print("\n__teste #", count)
            neighbors = kNN.get_neighbors(instancia_test, training_set, k)
            predict = kNN.predict_classification(neighbors)
            predictions.append(predict)

            print("Classe correta da instancia atual: {}, previsao: {}".format(instancia_test[-1], predict))
            analysis.add_matrix(instancia_test[-1], predict)

            if instancia_test[len(instancia_test) - 1] != predict:
                # SOMATORIA DOS ERROS DE CADA INSTANCIA (ERRO AMOSTRAL)
                sampling_error += 1

            count += 1
        erro_fold = cv.average_erros(sampling_error, number_of_instances)
        print("Erro Fold # {}: {}".format(i, erro_fold))
        training_set.clear()
    if number_of_classes > 2:
        analysis.show_multiclass_matrix()
        accuracy = analysis.calculate_accuracy()
        print("accuracy: ", accuracy)
    else:
        analysis.show_binary_matrix()
