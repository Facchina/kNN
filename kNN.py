from random import random
from operator import itemgetter
import math

class kNN:

    @staticmethod
    def generate_data_sets(data_set, split):
        """ Divide o conjunto de dados em conjunto de teste e treino.
            O parâmetro split corresponde a proporção aproximada em que os dados serão divididos
            Onde o valor do divisor será a porcentagem atribuída [0 a 1] ao treino e o restante ao teste
        """
        training = []
        test = []
        for i in range(1, len(data_set)):
            # ARMAZENANDO AS INSTANCIAS NOS CONJUNTOS
            if random() < split:
                training.append(data_set[i])
            else:
                test.append(data_set[i])
        # retorna as duas listas
        return training, test

    @staticmethod
    def euclidean_distance(test_instance, training_instance):
        """ Calcula a distancia entre as duas instências levando em conta as suas n-dimensões.
            A dimensões de uma instância corresponde a quantidade de atributos
        """
        difference_square = 0.0
        dimension = len(test_instance)
        for attribute in range(dimension):
            # somando o quadrado da diferença de cada atributo
            difference_square += pow((test_instance[attribute] - training_instance[attribute]), 2)

        distance = math.sqrt(difference_square)
        return distance

    @staticmethod
    def get_neighbors(instance, training_set, k_neighbors):
        """ Calcula a distancia de uma dada instancia em relação ao conjunto de treino
            Retorna os k vizinhos mais próximos dessa instancia
        """
        # SE A INSTANCIA TEM O MESMO TAMANHO QUE AS DO TREINO
        if not len(instance) - 1 == len(training_set[0]) - 1:
            print("The instance {} is invalid", instance)
            return
        # CONVERTE OS VALORES DA INSTANCIA PARA FLOAT
        for i in range(len(instance)):
            if not type(instance[i]) == float:
                try:
                    instance[i] = float(instance[i])
                except ValueError:
                    print("could not convert {} to float".format(instance[i]))
                    return
        distances = []
        for training_instance in training_set:
            # CALCULA A DISTANCIA DA INSTANCIA PASSADA EM RELAÇÃO AS INSTANCIAS DO CONJUNTO TREINO
            distance = kNN.euclidean_distance(instance, training_instance)
            # ARMANZENA AS INSTANCIAS DO CONJUNTO TREINO E A DISTANCIA CALCULADA EM RELAÇÃO A INSTANCIA PASSADA
            distances.append((training_instance, distance))

        # ORDENANDO A LISTA DE TRUPLAS EM ORDEM CRESCENTE DE DISTANCIA
        distances = sorted(distances, key=itemgetter(1))
        top_k = distances[:k_neighbors]
        # RETORNA OS K VIZINHOS MAIS PRÓXIMOS A INSTANCIA
        return top_k

    @staticmethod
    def predict_classification(neighbors):
        """"
            classifica a instancia baseada nos vizinhos mais proximos a ela
        """
        # DICIONARIO QUE ARMAZENA AS CLASSES E AS VEZES QUE ELAS APARECEM
        votes = {}
        # PERCORRE OS VIZINHOS ESCOLHIDOS
        for vizinho in range(len(neighbors)):
            instancia = neighbors[vizinho][0]
            classe = instancia[-1]
            # ARMANZENA A QUANTIDADE DE VOTOS PARA CADA CLASSE
            if classe in votes:
                votes[classe] += 1
            else:
                votes[classe] = 1
        print("quantidade de votos por cada classe: ", votes)

        #ORDENA DE ACORDO COM OS VOTOS E RETORNA UMA LISTA
        votes = sorted(votes.items(), key=itemgetter(1), reverse=True)
        mais_votos = votes[0][1]

        # VERIFICA SE TEM MAIS DE UMA CLASSE COM A MSM QUANTIDADE DE VOTOS (FALTA TERMINAR)
        popular_classes = []
        for class_votes in votes:
            if class_votes[1] >= mais_votos:
                print("Essa classe tem a maior quantidade votos")
                popular_classes.append(class_votes)
        print(popular_classes)

        if len(popular_classes) == 1:
            return popular_classes[0][0]
        else:
            print("desempate entre as classes com mais votos!")
            result = kNN.desempatar_classes_mais_votadas(popular_classes, neighbors)
            return result

    @staticmethod
    def desempatar_classes_mais_votadas(popular_classes, vizinhos):
        """"
            Funcao destinada a desempatar quando as classes recebem a mesma quantidade de votos.
            As distancias de cada instancia pertencentes a sua respectiva classe são somadas
            E a classe que tiver a menor distancia é a escolhida
        """
        sum_distances = {}
        for class_votes in popular_classes:
            for vizinho_dist in vizinhos:
                if vizinho_dist[0][-1] == class_votes[0]:
                    if class_votes[0] in sum_distances:
                        # soma a distancias de todos vizinhos da mesma classe
                        sum_distances[class_votes[0]] += vizinho_dist[1]
                    else:
                        sum_distances[class_votes[0]] = vizinho_dist[1]

        sum_distances = sorted(sum_distances.items(), key=itemgetter(1))
        return sum_distances[0][0]

