import math

import numpy as np


class CrossValidation:

    @staticmethod
    def split_dataset(data_set, qtd_particoes):

        """ Divide o conjunto de dados em conjunto de teste e treino.
            O parâmetro split corresponde a proporção aproximada em que os dados serão divididos
            Onde o valor do divisor será a porcentagem atribuída [0 a 1] ao treino e o restante ao teste
        """
        particoes = []
        linhas_por_particoes = math.floor((len(data_set)-1)/qtd_particoes)
        #print(linhas_por_particoes)
        fim = linhas_por_particoes + 1
        inc = linhas_por_particoes

        for ini in range(1, len(data_set), inc):

            if (ini + inc) >= len(data_set):
                fim = len(data_set)
                particoes.append(data_set[ini:fim])
            else:
                particoes.append(data_set[ini:fim])

            #print("\n", "inicio: {} fim: {}".format(ini, fim))
            fim += inc

        # count = 0
        # for i in range(len(particoes)):
        #     #print(particoes[i])
        #     count += len(particoes[i])
        #     #print(len(particoes[i]))
        # #print(count)

        return particoes

    @staticmethod
    def average_erros(erros, total_votes):

        average_erros = erros / total_votes
        return average_erros
