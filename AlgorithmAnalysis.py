import numpy


class AlgorithmAnalysis:
    table = []
    points = 0
    diagonal = 0

    def show_binary_matrix(self):

        tp = self.table[1][1]
        fp = self.table[1][2]
        fn = self.table[2][1]
        tn = self.table[2][2]

        table_str = self.table.astype(numpy.str)
        table_str[0][0] = "*"

        # ALTERA OS ROTULOS DA MATRIZ PARA P OU N
        table_str[0][1] = "P"
        table_str[0][2] = "N"
        table_str[1][0] = "P"
        table_str[2][0] = "N"

        print("\n\nMATRIZ DE CONFUSÃO (BINARIA):\n")
        print(table_str)

        # PROBABILIDADE DE SER VERDADEIRO POSITIVO
        sensitivity = tp/(tp+fn)
        # PROBABILIDADE DE SER VERDADEIRO NEGATIVO
        specificity = tn/(tn+fp)
        # QUANTOS ITENS QUE FORAM SELECIONADOS SAO RELEVANTES?
        precision = tp/(tp+fp)
        # QUANTOS ITENS RELEVANTES FORAM SELECIONADOS?
        recall = tp/(tp+fn)

        print("\n sensitivity: {}, specificity: {}, precision: {}, recall: {}".
              format(sensitivity, specificity, precision, recall))

    def confusion_matrix(self, number_of_classes):
        d = number_of_classes+1
        self.table = numpy.zeros((d, d), dtype=int)
        for i in range(len(self.table)):
            self.table[0][i] = i
            self.table[i][0] = i
        self.table[0][0] = -1

    def add_matrix(self, correct, predict):
        self.points += 1
        # IGNORANDO A COLUNA E LINHA DOS ROTULOS
        n = int(correct)
        m = int(predict)

        if correct == predict:
            self.table[n][n] += 1
            self.diagonal += 1
        else:
            self.table[n][m] += 1

    def show_multiclass_matrix(self):

        table_str = self.table.astype(numpy.str)
        table_str[0][0] = "*"

        print("\n\nMATRIZ DE CONFUSÃO (MULTICLASSES):\n")
        print(table_str)

    def calculate_accuracy(self):
        return self.diagonal / self.points